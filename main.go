package main

import (
	"gitlab.com/charlestenorios/prometheus_users-api/app"
)

func main() {
	app.StartApplication()

}
